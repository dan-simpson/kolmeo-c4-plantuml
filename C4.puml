' C4-PlantUML

' Kolmeo Setup
' ##################################

!include https://bitbucket.org/dan-simpson/kolmeo-c4-plantuml/raw/kolmeo/C4_KolmeoSettings.puml

' Colors
' ##################################

!global $White = "#FFFFFF"
!global $Black = "#000000"

!global $ELEMENT_FONT_COLOR = $White
!global $ELEMENT_FONT_COLOR_DARK = $YvesKleinBlue
!global $LEGEND_FONT_COLOR = $White
!global $LEGEND_TITLE_COLOR = $Black

' Styling
' ##################################

!global $TECHN_FONT_SIZE = 12

skinparam defaultTextAlignment center

skinparam wrapWidth 200
skinparam maxMessageSize 150

skinparam LegendBorderColor transparent
skinparam LegendBackgroundColor transparent
skinparam LegendFontColor $LEGEND_FONT_COLOR

skinparam rectangle {
    StereotypeFontSize 12
    shadowing false
    BorderColor transparent
}

skinparam database {
    StereotypeFontSize 12
    shadowing false
    BorderColor transparent
}

skinparam queue {
    StereotypeFontSize 12
    shadowing false
    BorderColor transparent
}

skinparam Arrow {
    Color $Black
    FontColor $Black
    FontSize 12
}

skinparam rectangle<<boundary>> {
    Shadowing false
    StereotypeFontSize 0
    FontColor $Black
    BorderColor $Black
    BorderStyle dashed
}

' Layout
' ##################################

!procedure HIDE_STEREOTYPE()
hide stereotype
!endprocedure

!procedure LAYOUT_AS_SKETCH()
skinparam backgroundColor $Kitti
skinparam handwritten true
skinparam defaultFontName "Comic Sans MS"
center footer <font color=red>Warning:</font> Created for discussion, needs to be validated
!endprocedure

!procedure LAYOUT_TOP_DOWN()
top to bottom direction
!endprocedure

!procedure LAYOUT_LEFT_RIGHT() 
left to right direction
!endprocedure

' Boundaries
' ##################################

!unquoted function Boundary($alias, $label)
!return 'rectangle "=='+$label+'" <<boundary>> as '+$alias
!endfunction
!unquoted function Boundary($alias, $label, $type)
!return 'rectangle "=='+$label+'\n<size:'+$TECHN_FONT_SIZE+'>['+$type+']</size>" <<boundary>> as '+$alias
!endfunction

' Relationship
' ##################################

!$relationshipIndex = 0
!$autoNumberRelationships = 0

!procedure AUTONUMBER_RELATIONSHIPS()
  !$autoNumberRelationships = 1
!endprocedure

!procedure INCREMENT_RELATIONSHIP_INDEX()
  !$relationshipIndex = $relationshipIndex + 1
!endprocedure

!unquoted procedure Rel_($alias1, $alias2, $label, $direction="")
!if ($autoNumberRelationships)
  INCREMENT_RELATIONSHIP_INDEX()
  $alias1 $direction $alias2 : **$relationshipIndex: $label**
!else
  $alias1 $direction $alias2 : **$label**
!endif
!endprocedure

!unquoted procedure Rel_($alias1, $alias2, $label, $techn, $direction="")
!if ($autoNumberRelationships)
  INCREMENT_RELATIONSHIP_INDEX()
  $alias1 $direction $alias2 : **$relationshipIndex. $label**\n//<size:$TECHN_FONT_SIZE>[$techn]</size>//
!else
  $alias1 $direction $alias2 : **$label**\n//<size:$TECHN_FONT_SIZE>[$techn]</size>//
!endif
!endprocedure

!unquoted procedure Rel($from, $to, $label)
Rel_($from, $to, $label, "-->>")
!endprocedure
!unquoted procedure Rel($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "-->>")
!endprocedure

!unquoted procedure BiRel($from, $to, $label)
Rel_($from, $to, $label, "<<-->>")
!endprocedure
!unquoted procedure BiRel($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "<<-->>")
!endprocedure

!unquoted procedure Rel_Back($from, $to, $label)
Rel_($from, $to, $label, "<<--")
!endprocedure
!unquoted procedure Rel_Back($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "<<--")
!endprocedure

!unquoted procedure Rel_Neighbor($from, $to, $label)
Rel_($from, $to, $label, "->>")
!endprocedure
!unquoted procedure Rel_Neighbor($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "->>")
!endprocedure

!unquoted procedure Rel_Back_Neighbor($from, $to, $label)
Rel_($from, $to, $label, "<<-")
!endprocedure
!unquoted procedure Rel_Back_Neighbor($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "<<-")
!endprocedure

!unquoted procedure Rel_D($from, $to, $label)
Rel_($from, $to, $label, "-DOWN->>")
!endprocedure
!unquoted procedure Rel_D($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "-DOWN->>")
!endprocedure
!unquoted procedure Rel_Down($from, $to, $label)
Rel_($from, $to, $label, "-DOWN->>")
!endprocedure
!unquoted procedure Rel_Down($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "-DOWN->>")
!endprocedure

!unquoted procedure BiRel_D($from, $to, $label)
Rel_($from, $to, $label, "<<-DOWN->>")
!endprocedure
!unquoted procedure BiRel_D($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "<<-DOWN->>")
!endprocedure
!unquoted procedure BiRel_Down($from, $to, $label)
Rel_($from, $to, $label, "<<-DOWN->>")
!endprocedure
!unquoted procedure BiRel_Down($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "<<-DOWN->>")
!endprocedure

!unquoted procedure Rel_U($from, $to, $label)
Rel_($from, $to, $label, "-UP->>")
!endprocedure
!unquoted procedure Rel_U($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "-UP->>")
!endprocedure
!unquoted procedure Rel_Up($from, $to, $label)
Rel_($from, $to, $label, "-UP->>")
!endprocedure
!unquoted procedure Rel_Up($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "-UP->>")
!endprocedure

!unquoted procedure BiRel_U($from, $to, $label)
Rel_($from, $to, $label, "<<-UP->>")
!endprocedure
!unquoted procedure BiRel_U($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "<<-UP->>")
!endprocedure
!unquoted procedure BiRel_Up($from, $to, $label)
Rel_($from, $to, $label, "<<-UP->>")
!endprocedure
!unquoted procedure BiRel_Up($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "<<-UP->>")
!endprocedure

!unquoted procedure Rel_L($from, $to, $label)
Rel_($from, $to, $label, "-LEFT->>")
!endprocedure
!unquoted procedure Rel_L($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "-LEFT->>")
!endprocedure
!unquoted procedure Rel_Left($from, $to, $label)
Rel_($from, $to, $label, "-LEFT->>")
!endprocedure
!unquoted procedure Rel_Left($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "-LEFT->>")
!endprocedure

!unquoted procedure BiRel_L($from, $to, $label)
Rel_($from, $to, $label, "<<-LEFT->>")
!endprocedure
!unquoted procedure BiRel_L($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "<<-LEFT->>")
!endprocedure
!unquoted procedure BiRel_Left($from, $to, $label)
Rel_($from, $to, $label, "<<-LEFT->>")
!endprocedure
!unquoted procedure BiRel_Left($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "<<-LEFT->>")
!endprocedure

!unquoted procedure Rel_R($from, $to, $label)
Rel_($from, $to, $label, "-RIGHT->>")
!endprocedure
!unquoted procedure Rel_R($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "-RIGHT->>")
!endprocedure
!unquoted procedure Rel_Right($from, $to, $label)
Rel_($from, $to, $label, "-RIGHT->>")
!endprocedure
!unquoted procedure Rel_Right($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "-RIGHT->>")
!endprocedure

!unquoted procedure BiRel_R($from, $to, $label)
Rel_($from, $to, $label, "<<-RIGHT->>")
!endprocedure
!unquoted procedure BiRel_R($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "<<-RIGHT->>")
!endprocedure
!unquoted procedure BiRel_Right($from, $to, $label)
Rel_($from, $to, $label, "<<-RIGHT->>")
!endprocedure
!unquoted procedure BiRel_Right($from, $to, $label, $techn)
Rel_($from, $to, $label, $techn, "<<-RIGHT->>")
!endprocedure

' Layout Helpers
' ##################################

!unquoted procedure Lay_D($from, $to)
$from -[hidden]D- $to
!endprocedure
!unquoted procedure Lay_U($from, $to)
$from -[hidden]U- $to
!endprocedure
!unquoted procedure Lay_R($from, $to)
$from -[hidden]R- $to
!endprocedure
!unquoted procedure Lay_L($from, $to)
$from -[hidden]L- $to
!endprocedure